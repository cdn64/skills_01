import {shallow} from "enzyme"
import React from "react"
import renderer from "react-test-renderer"

import List from "../presentational/List.js"
import DoubleList from "../presentational/DoubleList.js"
import EvenList from "../presentational/EvenList.js"
import Max from "../presentational/Max.js"

const numbers = [4, 8, 15, 16, 23, 42]
const evenNumbersCount = 4

describe("List", () => {
  it("renders a list of all given values", () => {
    const list = shallow(<List values={numbers} />)

    expect(list.find("ul").length).toEqual(1)
    expect(list.find("li").length).toEqual(numbers.length)
    expect(
      parseInt(
        list
          .find("li")
          .at(0)
          .text()
      )
    ).toEqual(numbers[0])
  })
})

describe("DoubleList", () => {
  it("renders a list of all given values, doubled in size", () => {
    const list = shallow(<DoubleList values={numbers} />)

    expect(list.find("ul").length).toEqual(1)
    expect(list.find("li").length).toEqual(numbers.length)
    expect(
      parseInt(
        list
          .find("li")
          .at(0)
          .text()
      )
    ).toEqual(numbers[0] * 2)
  })
})

describe("EvenList", () => {
  it("renders a list of all even values", () => {
    const list = shallow(<EvenList values={numbers} />)

    expect(list.find("ul").length).toEqual(1)
    expect(list.find("li").length).toEqual(evenNumbersCount)
  })
})

describe("Max", () => {
  it("calculates the maximum of given values", () => {
    const max = shallow(<Max values={numbers} />)

    expect(max.find("p").text()).toEqual("42")
  })
})
